include(CheckCXXSourceCompiles)

set(CMAKE_REQUIRED_FLAGS "-pedantic-errors")

check_cxx_source_compiles("
  #include <iostream>
  #include <tuple>
  int main()
  {
    auto tup = std::make_tuple(0, 'a', 3.14);
    for... (auto elem : tup)
      std::cout << elem << std::endl;
  }
"  AMDIS_HAS_EXPANSION_STATEMENTS
)

unset(CMAKE_REQUIRED_FLAGS)