#pragma once

/**
  * \defgroup GridFunctions GridFunction module
  * \brief Defines GridFunctions to be used in operators, boundary-conditions,
  * interpolation and integration.
  *
  * GridFunctions are expressions build up of some elementary terms and can be
  * used to construct a \ref GridFunctionOperator, can be interpolated to a
  * \ref DOFVector, and can be integrated over a GridView.
  *
  * Thus, GridFunctions are an important incredient to formulate the bilinear
  * and linear forms und to postprocess the solutions.
  *
  * **Examples:**
  * 1. Usage of GridFunctions to build Operators:
  * ```
  * ProblemStat<Traits> prob("name");
  * prob.initialize(INIT_ALL);
  *
  * auto opB = makeOperator(BiLinearForm, Expression);
  * prob.addMatrixOperator(opB, Row, Col);
  *
  * auto opL = makeOperator(LinearForm, Expression);
  * prob.addVectorOperator(opL, Row);
  * ```
  *
  * 2. Usage of GridFunctions in BoundaryConditions:
  * ```
  * prob.addDirichletBC(Predicate, Row, Col, Expression);
  * ```
  *
  * 3. Interpolate a GridFunction to a DOFVector:
  * ```
  * prob.solution(_0).interpol(Expression);
  * ```
  *
  * 4. Integrate a GridFunction on a GridView:
  * ```
  * auto value = integrate(Expression, prob.gridView());
  * ```
  *
  * **Remarks:**
  * - An `Expression` is anything, a GridFunction can be created from, sometimes
  *   also called PreGridFunction. It includes constants, functors callable with
  *   GlobalCoordinates, and any combination of GridFunctions.
  * - Anything that needs a quadrature formula, e.g., makeOperator() and
  *   integrate(), needs to determine the (approximative) polynomial degree of
  *   the GridFunctions. If the Gridfunction builds a polynomial expression, it
  *   can be deduced automatically, i.e. if it includes constants, DOFVectors,
  *   and arithmetic operator operator+, operator-, or operator*.
  *
  *   If the polynomial order can not be deduced, the compiler gives an error.
  *   Then, these functions accept an additional argument, to provide either the
  *   polynomial degree of the expression, or a quadrature rule explicitly.
  *
  *   *Examples:*
  *   + `auto op1 = makeOperator(B, 1.0 + pow<2>(prob.solution(_0)));`
  *   + `auto op2 = makeOperator(B, sin(X(0)), 4);`
  *   + `auto op3 = makeOperator(B, sin(X(0)), Dune::QuadratureRules(Dune::GeometryType::simplex, 4));`
  *   + `auto value1 = integrate(sin(X(0)), 4);`
  **/

#include <amdis/gridfunctions/AnalyticGridFunction.hpp>
#include <amdis/gridfunctions/ComposerGridFunction.hpp>
#include <amdis/gridfunctions/ConstantGridFunction.hpp>
#include <amdis/gridfunctions/CoordsGridFunction.hpp>
#include <amdis/gridfunctions/DerivativeGridFunction.hpp>
#include <amdis/gridfunctions/Operations.hpp>
