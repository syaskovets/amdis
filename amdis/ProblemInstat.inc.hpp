#pragma once

#include <string>
#include <vector>

#include <amdis/AdaptInfo.hpp>

namespace AMDiS {

template <class Traits>
void ProblemInstat<Traits>::transferInitialSolution(AdaptInfo& adaptInfo)
{
  AMDIS_FUNCNAME("ProblemInstat::transferInitialSolution()");

  test_exit(adaptInfo.time() == adaptInfo.startTime(),
            "after initial solution: time != start time");
  problemStat_->writeFiles(adaptInfo, true);
}


template <class Traits>
void ProblemInstat<Traits>::closeTimestep(AdaptInfo& adaptInfo)
{
  bool force = (adaptInfo.time() >= adaptInfo.endTime());
  problemStat_->writeFiles(adaptInfo, force);
}


template <class Traits>
void ProblemInstat<Traits>::initialize(Flag initFlag)
{
  // create vector for old solution
  if (initFlag.isSet(INIT_UH_OLD))
    createUhOld();
}


template <class Traits>
void ProblemInstat<Traits>::createUhOld()
{
  AMDIS_FUNCNAME("ProblemInstat::createUhOld()");

  if (oldSolution_)
    warning("oldSolution already created\n");
  else // create oldSolution
    oldSolution_.reset(new SolutionVector(*problemStat_->solutionVector()));
}


template <class Traits>
void ProblemInstat<Traits>::initTimestep(AdaptInfo&)
{
  if (oldSolution_)
    *oldSolution_ = *problemStat_->solutionVector();
}

} // end namespace AMDiS
