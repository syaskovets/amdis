#pragma once

#include <memory>
#include <type_traits>

#include <dune/common/unused.hh>
#include <dune/common/typeutilities.hh>

#include <amdis/Output.hpp>

namespace AMDiS
{
  /// Dummy implementation for sequential communication
  class SequentialCommunication
  {
  public:
    using Impl = SequentialCommunication;

    template <class T>
    SequentialCommunication(T&&) {}

    Impl const& get() const
    {
      return *this;
    }

    template <class Basis>
    void update(Basis const&) { /* do nothing */ }
  };


  template <class C>
  struct DefaultCommunicationCreator
  {
    using Communication = C;

    template <class Basis>
    static Communication create(Basis const& basis)
    {
      return {Environment::comm()};
    }
  };


  /// Implementation of a creator pattern for Communication types
  template <class C>
  struct CommunicationCreator
  {
    using Communication = C;

    template <class Basis>
    static Communication create(Basis const& basis, [[maybe_unused]] std::string const& prefix = "")
    {
      return DefaultCommunicationCreator<C>::create(basis);
    }
  };

} // end namespace AMDiS
