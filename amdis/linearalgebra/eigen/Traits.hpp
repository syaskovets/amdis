#pragma once

#include <dune/grid/common/partitionset.hh>
#include <amdis/linearalgebra/Communication.hpp>
#include <amdis/linearalgebra/eigen/MatrixSize.hpp>
#include <amdis/linearalgebra/eigen/MatrixBackend.hpp>
#include <amdis/linearalgebra/eigen/VectorBackend.hpp>

namespace AMDiS
{
  /** Traits class for a linear solver for the system AX=B using an FE space described by a dune-functions Basis
   *  Contains typedefs specific to the EIGEN backend.
   */
  struct EigenTraits
  {
    template <class Value>
    using MatrixImpl = EigenSparseMatrix<Value, Eigen::RowMajor>;

    template <class Value>
    using VectorImpl = EigenVector<Value>;

    using PartitionSet = Dune::Partitions::All;
    using SparsityPattern = MatrixSize;
    using Comm = SequentialCommunication;
  };

  template <class>
  using BackendTraits = EigenTraits;

} // end namespace AMDiS
