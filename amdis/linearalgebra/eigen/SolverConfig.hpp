#pragma once

#include <string>

#include <Eigen/UmfPackSupport>
#include <unsupported/Eigen/IterativeSolvers>

#include <amdis/Initfile.hpp>

namespace AMDiS
{
  template <class Solver>
  struct SolverConfig
  {
    template <class D>
    static void init(std::string const& prefix, Eigen::IterativeSolverBase<D>& solver)
    {
      using RealScalar = typename Eigen::IterativeSolverBase<D>::RealScalar;
      auto rtol = Parameters::get<RealScalar>(prefix + "->relative tolerance");
      if (rtol)
        solver.setTolerance(rtol.value());

      int maxIter = 500;
      Parameters::get(prefix + "->max iteration", maxIter);
      solver.setMaxIterations(maxIter);
    }

    // fall-back default implementation
    template <class D>
    static void init(std::string const&, Eigen::SparseSolverBase<D>&) {}
  };

  template <class M, class P>
  struct SolverConfig<Eigen::GMRES<M,P>>
  {
    using Base = Eigen::IterativeSolverBase<Eigen::GMRES<M,P>>;

    static void init(std::string const& prefix, Eigen::GMRES<M,P>& solver)
    {
      SolverConfig<Base>::init(prefix, solver);

      auto restart = Parameters::get<int>(prefix + "->restart");
      if (restart)
        solver.set_restart(restart.value());
    }
  };

  template <class M, class P>
  struct SolverConfig<Eigen::DGMRES<M,P>>
  {
    using Base = Eigen::IterativeSolverBase<Eigen::DGMRES<M,P>>;

    static void init(std::string const& prefix, Eigen::DGMRES<M,P>& solver)
    {
      SolverConfig<Base>::init(prefix, solver);

      auto restart = Parameters::get<int>(prefix + "->restart");
      if (restart)
        solver.set_restart(restart.value());

      // number of eigenvalues to deflate at each restart
      auto neigs = Parameters::get<int>(prefix + "->num eigenvalues");
      if (neigs)
        solver.setEigenv(neigs.value());

      // maximum size of the deflation subspace
      auto maxNeig = Parameters::get<int>(prefix + "->max num eigenvalues");
      if (maxNeig)
        solver.setMaxEigenv(maxNeig.value());
    }
  };

#if HAVE_SUITESPARSE_UMFPACK
  template <class M>
  struct SolverConfig<Eigen::UmfPackLU<M>>
  {
    static void init(std::string const& prefix, Eigen::UmfPackLU<M>& solver)
    {
      [[maybe_unused]] auto& control = solver.umfpackControl();
      // TODO: initialized umfpack parameters
    }
  };
#endif

} // end namespace AMDiS
