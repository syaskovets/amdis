#pragma once

#include <memory>

#include <amdis/CreatorInterface.hpp>
#include <amdis/Initfile.hpp>
#include <amdis/linearalgebra/LinearSolverInterface.hpp>
#include <amdis/linearalgebra/eigen/PreconConfig.hpp>
#include <amdis/linearalgebra/eigen/SolverConfig.hpp>

namespace AMDiS
{
  template <class M, class X, class Y, class IterativeSolver>
  class IterativeRunner
      : public LinearSolverInterface<M,X,Y>
  {
    using SolverCfg = SolverConfig<IterativeSolver>;
    using PreconCfg = PreconConfig<typename IterativeSolver::Preconditioner>;

  public:
    IterativeRunner(std::string const& prefix)
      : solver_{}
    {
      SolverCfg::init(prefix, solver_);
      PreconCfg::init(prefix + "->precon", solver_.preconditioner());
      Parameters::get(prefix + "->reuse pattern", reusePattern_);
    }

    /// initialize the matrix and maybe compute factorization
    void init(M const& A)
    {
      if (!reusePattern_ || !initialized_) {
        solver_.analyzePattern(A);
        initialized_ = true;
      }
      solver_.factorize(A);

      test_exit(solver_.info() == Eigen::Success, "Error in solver.compute(matrix)");
    }

    /// Implements \ref LinearSolverInterface::finish()
    void finish()
    {
      initialized_ = false;
    }

    /// Implements \ref LinearSolverInterface::apply()
    void apply(X& x, Y const& b, Dune::InverseOperatorResult& stat) override
    {
      Dune::Timer t;
      x = solver_.solveWithGuess(b, x);

      stat.reduction = solver_.error();
      stat.iterations = solver_.iterations();
      stat.converged = (solver_.info() == Eigen::Success);
      stat.elapsed = t.elapsed();
    }

  private:
    IterativeSolver solver_;
    bool reusePattern_ = false;
    bool initialized_ = false;
  };

} // end namespace AMDiS
