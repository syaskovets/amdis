#pragma once

#include <memory>
#include <optional>
#include <string>

#include <dune/istl/owneroverlapcopy.hh>
#include <dune/istl/solvercategory.hh>
#include <dune/istl/paamg/pinfo.hh>

#include <amdis/Environment.hpp>
#include <amdis/functions/GlobalIdSet.hpp>
#include <amdis/linearalgebra/Communication.hpp>
#include <amdis/linearalgebra/ParallelIndexSet.hpp>

namespace AMDiS
{
  /// Dummy implementation for ISTL-specific communication when no MPI is found
  template <class G, class L>
  class SequentialISTLCommunication
  {
    using Impl = Dune::Amg::SequentialInformation;

  public:
    using Sequential = SequentialISTLCommunication;

    SequentialISTLCommunication() = default;

    template <class Basis>
    SequentialISTLCommunication(Basis const&, typename Dune::SolverCategory::Category cat)
    {
      assert(cat == Dune::SolverCategory::sequential);
    }

    typename Dune::SolverCategory::Category category() const
    {
      return Dune::SolverCategory::sequential;
    }

    Impl const& get() const
    {
      return impl_;
    }

    Sequential const& sequential() const
    {
      return *this;
    }

    template <class Basis>
    void update(Basis const&) { /* do nothing */ }

  private:
    Impl impl_;
  };


#if HAVE_MPI
  /// Implementation class for ISTL-specific communication to be used in parallel solvers
  template <class GlobalId, class LocalIndex>
  class ISTLCommunication
  {
    using Impl = Dune::OwnerOverlapCopyCommunication<GlobalId, LocalIndex>;

  public:
    using Sequential = SequentialISTLCommunication<GlobalId, LocalIndex>;
    using IndexSet = typename Impl::ParallelIndexSet;
    using RemoteIndices = typename Impl::RemoteIndices;

  public:
    template <class Basis>
    ISTLCommunication(Basis const& basis, typename Dune::SolverCategory::Category cat)
      : cat_(cat)
    {
      update(basis);
    }

    IndexSet const& indexSet() const
    {
      assert(bool(impl_));
      return impl_->indexSet();
    }

    RemoteIndices const& remoteIndices() const
    {
      assert(bool(impl_));
      return impl_->remoteIndices();
    }

    typename Dune::SolverCategory::Category category() const
    {
      return cat_;
    }

    Impl const& get() const
    {
      assert(bool(impl_));
      return *impl_;
    }

    Sequential sequential() const
    {
      return Sequential{};
    }

    template <class Basis>
    void update(Basis const& basis)
    {
      impl_ = std::make_unique<Impl>(Environment::comm(), cat_);

      auto& pis = impl_->indexSet();
      buildParallelIndexSet(basis, pis);

      auto& ris = impl_->remoteIndices();
      ris.setIndexSets(pis, pis, impl_->communicator());
      ris.template rebuild<true>();
    }

  private:
    typename Dune::SolverCategory::Category cat_;
    std::unique_ptr<Impl> impl_ = nullptr;
  };

#else // !HAVE_MPI

  template <class G, class L>
  using ISTLCommunication = SequentialISTLCommunication<G,L>;

#endif

  template <class B>
  using ISTLCommunication_t
    = ISTLCommunication<GlobalIdType_t<B>, typename B::size_type>;


  template <class G, class L>
  struct CommunicationCreator<ISTLCommunication<G,L>>
  {
    using Communication = ISTLCommunication<G,L>;

    /**
     * ISTLCommunication creator function. Takes the global basis and the solver's initfile prefix
     * and returns a unique pointer to the ISTLCommunication object.
     * Deduces the category of the solver used from the initfile and basis
     *
     * \param basis   Global basis associated to the solvers' matrices and vectors
     * \param prefix  Solver prefix used in the initfile, usually "<problem name>->solver"
     **/
    template <class Basis>
    static Communication create(Basis const& basis, std::string const& prefix = "");
  };

} // end namespace AMDiS

#include <amdis/linearalgebra/istl/Communication.inc.hpp>
