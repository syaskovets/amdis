#include <dune/common/version.hh>
#include <dune/istl/solvers.hh>

#if HAVE_SUITESPARSE_UMFPACK
#include <dune/istl/umfpack.hh>
#endif
#if HAVE_SUITESPARSE_LDL
#include <dune/istl/ldl.hh>
#endif
#if HAVE_SUITESPARSE_SPQR
#include <dune/istl/spqr.hh>
#endif
#if HAVE_SUPERLU
#include <dune/istl/superlu.hh>
#endif

#include <amdis/linearalgebra/istl/precompiled/Common.hpp>

namespace Dune
{
  // iterative solver
  extern template class CGSolver<Precompiled::Vector>;
  extern template class GeneralizedPCGSolver<Precompiled::Vector>;
  extern template class BiCGSTABSolver<Precompiled::Vector>;
  extern template class MINRESSolver<Precompiled::Vector>;
  extern template class RestartedGMResSolver<Precompiled::Vector>;
  extern template class RestartedFCGSolver<Precompiled::Vector>;
  extern template class CompleteFCGSolver<Precompiled::Vector>;
  extern template class RestartedFlexibleGMResSolver<Precompiled::Vector>;

  // direct solver
#if HAVE_SUITESPARSE_UMFPACK
  extern template class UMFPack<Precompiled::Matrix>;
#endif

#if HAVE_SUITESPARSE_LDL
  extern template class LDL<Precompiled::Matrix>;
#endif

#if HAVE_SUITESPARSE_SPQR
  extern template class SPQR<Precompiled::Matrix>;
#endif

} // end namespace Dune
