#include <config.h>

#include "Preconditioners.hpp"
#include "Solvers.hpp"

namespace AMDiS
{
  // explicit template instantiation:
  template class DefaultCreators< tag::solver<Precompiled::SolverTraits> >;

} // end namespace AMDiS
