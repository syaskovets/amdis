#pragma once

#include <amdis/Initfile.hpp>
#include <amdis/Output.hpp>

namespace AMDiS {

#if HAVE_MPI

template <class G, class L>
  template <class Basis>
ISTLCommunication<G,L> CommunicationCreator<ISTLCommunication<G,L>>
  ::create(Basis const& basis, std::string const& prefix)
{
  using SolverType = Dune::SolverCategory::Category;
  std::string category = "";
  Parameters::get(prefix + "->category", category);
  if (category == "")
  {
    // check global parameter if no local one was found
    category = "default";
    Parameters::get("solver category", category);
  }
  SolverType cat = SolverType::sequential;
  auto const& gv = basis.gridView();
  int mpiSize = gv.comm().size();

  if (category == "default")
  {
    if (mpiSize == 1)
    {
      cat = SolverType::sequential;
    }
    else
    {
      // Use overlapping solver if grid has overlap
      if (gv.overlapSize(0) > 0)
        cat = SolverType::overlapping;
      else
      {
        // TODO(FM): Remove once nonoverlapping solvers are supported
        warning("Nonoverlapping solvers are currently not supported.");
        cat = SolverType::nonoverlapping;
      }
    }
  }
  else if (category != "sequential" && mpiSize == 1)
  {
    warning("Only one process detected. Solver category set to sequential\n");
    cat = SolverType::sequential;
  }
  else if (category == "sequential")
  {
    test_exit(mpiSize == 1, "Solver category sequential is not supported in parallel\n");
    cat = SolverType::sequential;
  }
  else if (category == "overlapping")
  {
    if (gv.overlapSize(0) == 0)
      warning("Overlapping solver category chosen for grid with no overlap\n");
    cat = SolverType::overlapping;
  }
  else if (category == "nonoverlapping")
  {
    // TODO(FM): Remove once nonoverlapping solvers are supported
    warning("Nonoverlapping solvers are currently not supported.");
    cat = SolverType::nonoverlapping;
  }
  else
  {
    error_exit("Unknown solver category\n");
  }

  return {basis, cat};
}

#else // HAVE_MPI

template <class G, class L>
  template <class Basis>
ISTLCommunication<G,L> CommunicationCreator<ISTLCommunication<G,L>>
  ::create(Basis const& /*basis*/, std::string const& /*prefix*/)
{
  return {};
}

#endif // HAVE_MPI

} // end namespace AMDiS
