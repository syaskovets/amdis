#pragma once

#include <algorithm>
#include <vector>

#include <boost/numeric/mtl/matrix/compressed2D.hpp>
#include <boost/numeric/mtl/matrix/inserter.hpp>
#include <boost/numeric/mtl/utility/property_map.hpp>
#include <boost/numeric/mtl/utility/range_wrapper.hpp>

#include <amdis/linearalgebra/Constraints.hpp>
#include <amdis/linearalgebra/SymmetryStructure.hpp>
#include <amdis/linearalgebra/mtl/MatrixBackend.hpp>
#include <amdis/linearalgebra/mtl/VectorBackend.hpp>

namespace AMDiS
{
  template <class T>
  struct Constraints<MTLSparseMatrix<T>>
  {
    using Matrix = MTLSparseMatrix<T>;
    using Vector = MTLVector<T>;

    template <class BitVector>
    static void dirichletBC(Matrix& mat, Vector& sol, Vector& rhs, BitVector const& nodes, bool setDiagonal = true)
    {
      SymmetryStructure const symmetry = mat.symmetry();
      if (symmetry == SymmetryStructure::spd || symmetry == SymmetryStructure::symmetric || symmetry == SymmetryStructure::hermitian)
        symmetricDirichletBC(mat.matrix(), sol.vector(), rhs.vector(), nodes, setDiagonal);
      else
        unsymmetricDirichletBC(mat.matrix(), sol.vector(), rhs.vector(), nodes, setDiagonal);
    }

    template <class Mat, class Vec, class BitVector>
    static void symmetricDirichletBC(Mat& mat, Vec& sol, Vec& rhs, BitVector const& nodes, bool setDiagonal = true)
    {
      // Define the property maps
      auto row   = mtl::mat::row_map(mat);
      auto col   = mtl::mat::col_map(mat);
      auto value = mtl::mat::value_map(mat);

      // iterate over the matrix
      std::size_t slotSize = 0;
      for (auto r : mtl::rows_of(mat)) {   // rows or columns
        std::size_t rowSize = 0;
        for (auto i : mtl::nz_of(r)) {          // non-zeros within
          ++rowSize;
          if (nodes[row(i)]) {
            // set identity row
            value(i, 0);
          }
          else if (setDiagonal && nodes[col(i)]) {
            rhs[row(i)] -= value(i) * sol[col(i)];
            value(i, 0);
          }
        }
        slotSize = std::max(slotSize, rowSize);
      }

      // set diagonal entry
      if (setDiagonal) {
        mtl::mat::inserter<Mat, mtl::update_store<typename Mat::value_type> > ins(mat, slotSize);
        for (std::size_t i = 0; i < nodes.size(); ++i) {
          if (nodes[i])
            ins[i][i] = 1;
        }
      }

      // copy solution dirichlet data to rhs vector
      for (typename Vec::size_type i = 0; i < mtl::size(sol); ++i) {
        if (nodes[i])
          rhs[i] = sol[i];
      }
    }

    template <class Mat, class Vec, class BitVector>
    static void unsymmetricDirichletBC(Mat& mat, Vec& sol, Vec& rhs, BitVector const& nodes, bool setDiagonal = true)
    {
      // Define the property maps
      auto row   = mtl::mat::row_map(mat);
      auto col   = mtl::mat::col_map(mat);
      auto value = mtl::mat::value_map(mat);

      // iterate over the matrix
      for (auto r : mtl::rows_of(mat)) {   // rows of the matrix
        if (nodes[r.value()]) {
          for (auto i : mtl::nz_of(r)) {          // non-zeros within
            // set identity row
            value(i, (setDiagonal && row(i) == col(i) ? 1 : 0) );
          }
        }
      }

      // copy solution dirichlet data to rhs vector
      for (typename Vec::size_type i = 0; i < mtl::size(sol); ++i) {
        if (nodes[i])
          rhs[i] = sol[i];
      }
    }



    template <class Associations>
    static std::size_t at(Associations const& m, std::size_t idx)
    {
      auto it = m.find(idx);
      assert(it != m.end());
      return it->second;
    }

    template <class BitVector, class Associations>
    static void periodicBC(Matrix& mat, Vector& sol, Vector& rhs, BitVector const& left, Associations const& left2right,
                           bool setDiagonal = true)
    {
      SymmetryStructure const symmetry = mat.symmetry();
      if (symmetry == SymmetryStructure::spd || symmetry == SymmetryStructure::symmetric || symmetry == SymmetryStructure::hermitian)
        symmetricPeriodicBC(mat.matrix(), sol.vector(), rhs.vector(), left, left2right, setDiagonal);
      else
        unsymmetricPeriodicBC(mat.matrix(), sol.vector(), rhs.vector(), left, left2right, setDiagonal);
    }


    template <class Mat, class Vec, class BitVector, class Associations>
    static void symmetricPeriodicBC(Mat& mat, Vec& sol, Vec& rhs, BitVector const& left, Associations const& left2right,
                                    bool setDiagonal = true)
    {
      error_exit("Not implemented");
    }


    template <class Value>
    struct Triplet
    {
      std::size_t row, col;
      Value value;
    };

    template <class Mat, class Vec, class BitVector, class Associations>
    static void unsymmetricPeriodicBC(Mat& mat, Vec& sol, Vec& rhs, BitVector const& left, Associations const& left2right,
                                      bool setDiagonal = true)
    {
      std::vector<Triplet<typename Mat::value_type>> rowValues;
      rowValues.reserve(left2right.size()*std::size_t(mat.nnz()/(0.9*num_rows(mat))));

      // Define the property maps
      // auto row   = mtl::mat::row_map(mat);
      auto col   = mtl::mat::col_map(mat);
      auto value = mtl::mat::value_map(mat);

      // iterate over the matrix
      std::size_t slotSize = 0;
      for (auto r : mtl::rows_of(mat)) {
        if (left[r.value()]) {
          slotSize = std::max(slotSize, std::size_t(mat.nnz_local(r.value())));
          std::size_t right = at(left2right,r.value());

          for (auto i : mtl::nz_of(r)) {
            rowValues.push_back({right,col(i),value(i)});
            value(i, 0);
          }
        }
      }

      mtl::mat::inserter<Mat, mtl::update_plus<typename Mat::value_type> > ins(mat, 2*slotSize);
      for (auto const& t : rowValues)
        ins[t.row][t.col] += t.value;

      for (std::size_t i = 0; i < mtl::size(left); ++i) {
        if (left[i]) {
          std::size_t j = at(left2right,i);
          if (setDiagonal) {
            ins[i][i] = 1;
            ins[i][j] = -1;
          }

          rhs[j] += rhs[i];
          rhs[i] = 0;

          sol[j] = sol[i];
        }
      }
    }

  };

} // end namespace AMDiS
