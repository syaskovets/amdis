#pragma once

#include <optional>

#include <petsc.h>

#include <dune/common/parallel/indexset.hh>
#include <dune/common/parallel/localindex.hh>
#include <dune/common/parallel/plocalindex.hh>
#include <dune/common/parallel/remoteindices.hh>
#include <dune/common/typeutilities.hh>

#include <amdis/functions/GlobalIdSet.hpp>
#include <amdis/linearalgebra/Communication.hpp>
#include <amdis/linearalgebra/DOFMapping.hpp>
#include <amdis/linearalgebra/ParallelIndexSet.hpp>

namespace AMDiS
{
  template <class GlobalId, class SizeType>
  class DistributedCommunication
  {
    using Self = DistributedCommunication;
  public:
#if HAVE_MPI
    using LocalIndex = Dune::ParallelLocalIndex<DefaultAttributeSet::Type>;
    using IndexSet = Dune::ParallelIndexSet<GlobalId, LocalIndex, 512>;
    using RemoteIndices = Dune::RemoteIndices<IndexSet>;
#else
    struct IndexSet
    {
      constexpr SizeType size() const { return size_; }
      SizeType size_ = 0;
    };

    struct RemoteIndices {};
#endif
    using DofMap = DofMapping<IndexSet, PetscInt>;

  public:
    template <class Basis,
      Dune::disableCopyMove<Self, Basis> = 0>
    DistributedCommunication(Basis const& basis)
      : remoteIndices_(std::make_unique<RemoteIndices>())
    {
      update(basis);
    }

    DistributedCommunication(Self const& other) = delete;
    DistributedCommunication(Self&& other) = default;

    /// Return the ParallelIndexSet
    IndexSet const& indexSet() const { return *indexSet_; }
    IndexSet&       indexSet()       { return *indexSet_; }

    /// Return the RemoteIndices
    RemoteIndices const& remoteIndices() const { return *remoteIndices_; }
    RemoteIndices&       remoteIndices()       { return *remoteIndices_; }

    /// Return the DofMapping
    DofMap const& dofMap() const { return dofMap_; }
    DofMap&       dofMap()       { return dofMap_; }

    /// Update the indexSet, remoteIndices and dofmapping
    template <class Basis>
    void update(Basis const& basis)
    {
      indexSet_ = std::make_unique<IndexSet>();

#if HAVE_MPI
      buildParallelIndexSet(basis, *indexSet_);
      remoteIndices_->setIndexSets(*indexSet_, *indexSet_, Environment::comm());
      remoteIndices_->template rebuild<true>();
#else
      (*indexSet_).size_ = basis.dimension();
#endif

      dofMap_.update(*this);
    }

  protected:
    std::unique_ptr<IndexSet> indexSet_ = nullptr;
    std::unique_ptr<RemoteIndices> remoteIndices_;
    DofMap dofMap_;
  };

  template <class B>
  using DistributedCommunication_t
    = DistributedCommunication<GlobalIdType_t<B>, typename B::size_type>;


  template <class G, class L>
  struct CommunicationCreator<DistributedCommunication<G,L>>
  {
    using Communication = DistributedCommunication<G,L>;

    template <class Basis>
    static Communication create(Basis const& basis, std::string const& prefix = "")
    {
      return {basis};
    }
  };

} // end namespace AMDiS
