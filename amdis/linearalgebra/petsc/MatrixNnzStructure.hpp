#pragma once

#include <algorithm>
#include <vector>

#include <amdis/common/Index.hpp>
#include <amdis/linearalgebra/SymmetryStructure.hpp>

#if HAVE_MPI
#include <amdis/common/parallel/Communicator.hpp>
#endif

namespace AMDiS
{
  /// Sparsity pattern used to create PETSc matrices
  class MatrixNnzStructure
  {
  public:
    template <class RowBasis, class ColBasis>
    MatrixNnzStructure(RowBasis const& rowBasis, ColBasis const& colBasis,
                       SymmetryStructure symmetry = SymmetryStructure::unknown)
      : symmetry_(symmetry)
    {
      init(rowBasis, colBasis);
    }

    // Return Number of nonzeros in the diagonal part (owner part)
    std::vector<PetscInt> const& d_nnz() const
    {
      return dnnz_;
    }

    /// Return Number of nonzeros in the off-diagonal part (overlap part)
    std::vector<PetscInt> const& o_nnz() const
    {
      return onnz_;
    }

    /// Symmetry of the matrix entries
    SymmetryStructure symmetry() const
    {
      return symmetry_;
    }

  protected:
    // Create pattern from basis
    template <class RowBasis, class ColBasis>
    void init(RowBasis const& rowBasis, ColBasis const& colBasis);

  private:
    std::vector<PetscInt> dnnz_; //< number of nonzeros in the diagonal part (owner part)
    std::vector<PetscInt> onnz_; //< number of nonzeros in the off-diagonal part (overlap part)
    SymmetryStructure symmetry_;

#if HAVE_MPI
    const Mpi::Tag tag_{916821}; //< Communication tag used internally
#endif
  };

} // end namespace AMDiS

#include <amdis/linearalgebra/petsc/MatrixNnzStructure.inc.hpp>
