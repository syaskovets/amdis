#pragma once

#include <amdis/linearalgebra/petsc/MatrixBackend.hpp>
#include <amdis/linearalgebra/petsc/VectorBackend.hpp>

namespace AMDiS {

  // ||b - A*x||
  template <class DofMap>
  auto residuum(PetscMatrix<DofMap> const& A, PetscVector<DofMap> const& x, PetscVector<DofMap> const& b)
  {
    Vec r;
    VecDuplicate(b.vector(), &r);
    MatMult(A.matrix(), x.vector(), r);
    VecAXPY(r, -1.0, b.vector());

    PetscReal res;
    VecNorm(r, NORM_2, &res);
    return res;
  }

  // ||b - A*x|| / ||b||
  template <class DofMap>
  auto relResiduum(PetscMatrix<DofMap> const& A, PetscVector<DofMap> const& x, PetscVector<DofMap> const& b)
  {
    PetscReal bNrm;
    VecNorm(b.vector(), NORM_2, &bNrm);
    return residuum(A,x,b) / bNrm;
  }

} // end namespace AMDiS
