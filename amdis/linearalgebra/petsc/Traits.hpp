#pragma once

#include <type_traits>

#include <petscmat.h>
#include <petscvec.h>

#include <dune/grid/common/partitionset.hh>
#include <amdis/linearalgebra/petsc/Communication.hpp>
#include <amdis/linearalgebra/petsc/MatrixNnzStructure.hpp>
#include <amdis/linearalgebra/petsc/MatrixBackend.hpp>
#include <amdis/linearalgebra/petsc/VectorBackend.hpp>

namespace AMDiS
{
  /** Traits class for a linear solver for the system AX=B using an FE space described by a dune-functions Basis
   *  Contains typedefs specific to the PETSc backend.
   */
  template <class Basis>
  struct PetscTraits
  {
    using Comm = DistributedCommunication_t<Basis>;

    template <class>
    using MatrixImpl = PetscMatrix<typename Comm::DofMap>;

    template <class>
    using VectorImpl = PetscVector<typename Comm::DofMap>;

    using PartitionSet = Dune::Partitions::Interior;
    using SparsityPattern = MatrixNnzStructure;
  };

  template <class Basis>
  using BackendTraits = PetscTraits<Basis>;

} // end namespace AMDiS
