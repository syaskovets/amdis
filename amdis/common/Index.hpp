#pragma once

// std c++ headers
#include <tuple>
#include <type_traits>
#include <utility>

namespace AMDiS
{
  // introduce some shortcuts for integral constants
  // ---------------------------------------------------------------------------

  /// A wrapper for int type
  template <int I>
  using int_t = std::integral_constant<int, I>;

  /// Variable template to generate int-type
  template <int I>
  constexpr int_t<I> int_ = {};

  /// class that represents a sequence of integers
  template <int... I>
  using Ints = std::integer_sequence<int, I...>;

  template <std::size_t I, int... J>
  auto get(Ints<J...>) { return std::get<I>(std::make_tuple(int_<J>...)); }


  /// A wrapper for std::size_t type
  template <std::size_t I>
  using index_t = std::integral_constant<std::size_t, I>;

  /// Variable template to generate std::size_t-type
  template <std::size_t I>
  constexpr index_t<I> index_ = {};


  /// class that represents a sequence of indices
  template <std::size_t... I>
  using Indices = std::index_sequence<I...>;

  template <std::size_t I, std::size_t... J>
  auto get(Indices<J...>) { return std::get<I>(std::make_tuple(index_<J>...)); }

} // end namespace AMDiS
