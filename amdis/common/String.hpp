#pragma once

#include <algorithm>
#include <cctype>
#include <locale>
#include <string>

namespace AMDiS
{
  /// convert all characters in a string to upper case
  std::string to_upper(std::string input);

  /// convert all characters in a string to upper case
  std::string to_lower(std::string input);

  /// trim a string from the left
  std::string& ltrim(std::string& str);

  /// trim a string from the right
  std::string& rtrim(std::string& str);

  /// trim a string from both sides
  std::string& trim(std::string& str);

  /// trim a (copy of the) string from both sides
  std::string trim_copy(std::string const& str);

  /// Replace all occurences of substring `from` with `to` in source `str`.
  void replaceAll(std::string& str, std::string const& from, std::string const& to);

} // end namspace AMDiS
