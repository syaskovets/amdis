#pragma once

#include <cassert>

#include <dune/common/hybridutilities.hh>
#include <dune/common/rangeutilities.hh>

namespace AMDiS
{
  // static switching. Iterates over all possible values
  template <class T, T to, T from, class Value, class Then, class Else>
  constexpr decltype(auto) switchCases(const Dune::StaticIntegralRange<T,to,from>& cases, const Value& value,
                                       Then&& thenBranch, Else&& elseBranch)
  {
    using integer_sequence = typename Dune::StaticIntegralRange<T,to,from>::integer_sequence;
    return Dune::Hybrid::switchCases(integer_sequence{}, value, FWD(thenBranch), FWD(elseBranch));
  }

  // dynamic switching. Calls the `thenBranch` or `elseBranch` directly
  template <class T, class Value, class Then, class Else>
  constexpr decltype(auto) switchCases(const Dune::IntegralRange<T>& cases, const Value& value,
                                       Then&& thenBranch, Else&& elseBranch)
  {
    if (value >= cases[0] && value <= cases[cases.size()-1])
      return thenBranch(value);
    else
      return elseBranch(value);
  }

  // static switching. Iterates over all possible values
  template<class T, T to, T from, class Value, class Then>
  constexpr void switchCases(const Dune::StaticIntegralRange<T,to,from>& cases, const Value& value, Then&& thenBranch)
  {
    using integer_sequence = typename Dune::StaticIntegralRange<T,to,from>::integer_sequence;
    Dune::Hybrid::switchCases(integer_sequence{}, value, FWD(thenBranch));
  }

  // dynamic switching. Calls the `thenBranch` directly
  template<class T, class Value, class Then>
  constexpr void switchCases(const Dune::IntegralRange<T>& cases, const Value& value, Then&& thenBranch)
  {
    assert(value >= cases[0] && value <= cases[cases.size()-1]);
    thenBranch(value);
  }

} // end namespace AMDiS
