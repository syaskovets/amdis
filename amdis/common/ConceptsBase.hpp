#pragma once

#include <type_traits>
#include <dune/common/typetraits.hh>

#define AMDIS_CONCAT_IMPL( x, y ) x##y
#define AMDIS_MACRO_CONCAT( x, y ) AMDIS_CONCAT_IMPL( x, y )

#ifdef DOXYGEN
  #define REQUIRES(...)
  #define REQUIRES_(...)
  #define CONCEPT constexpr
  #define CHECK_CONCEPT(...)
#else
  #define REQUIRES(...) std::enable_if_t<__VA_ARGS__ , int> = 0
  #define REQUIRES_(...) std::enable_if_t<__VA_ARGS__ , int>
  #define CONCEPT constexpr
  #define CHECK_CONCEPT(...) static __VA_ARGS__ AMDIS_MACRO_CONCAT( _concept_check_, __COUNTER__ )
#endif

namespace AMDiS
{
  namespace Concepts
  {
    namespace Impl_
    {
      template <class Concept, class = std::void_t<>>
      struct models
          : std::false_type
      {};

      template <class Concept, class... Ts>
      struct models<Concept(Ts...), std::void_t< decltype(std::declval<Concept>().require(std::declval<Ts>()...)) >>
          : std::true_type
      {};

    } // end namespace Impl_


#ifndef DOXYGEN
    template <class Concept>
    constexpr bool models = Impl_::models<Concept>::value;

    template <class Concept>
    using models_t = Impl_::models<Concept>;
#endif // DOXYGEN

  } // end namespace Concepts
} // end namespace AMDiS
