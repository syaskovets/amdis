#pragma once

#include <type_traits>

#include <amdis/common/ConceptsBase.hpp>

namespace AMDiS
{
  /// polynomial order of functions
  template <class F>
  auto order(F const& f)
    -> decltype(&F::operator(), f.order())
  {
    return f.order();
  }

  namespace Concepts
  {
    /** \addtogroup Concepts
     *  @{
     **/

    namespace Definition
    {
      struct Polynomial
      {
        template <class F>
        auto require(F&& f) -> decltype(
          order(f)
        );
      };

    } // end namespace Definition

    /// \brief Funcion F has free function `order(F)`
    template <class F>
    constexpr bool Polynomial = models<Definition::Polynomial(F)>;

    template <class F>
    using Polynomial_t = models_t<Definition::Polynomial(F)>;

    /** @} **/

  } // end namespace Concepts
} // end namespace AMDiS
