#pragma once

// std c++ headers
#include <tuple>
#include <type_traits>
#include <utility>

namespace AMDiS
{
  /// A wrapper for bool types
  template <bool B>
  using bool_t  = std::integral_constant<bool, B>;

  /// Variable template to generate bool type
  template <bool B>
  constexpr bool_t<B> bool_ = {};

  // some boolean operations
  // ---------------------------------------------------------------------------

  template <bool... Bs>
  constexpr bool all_of_v = (Bs && ...);

  template <bool... Bs>
  using all_of_t = bool_t<all_of_v<Bs...>>;

  template <bool... Bs>
  using and_t = all_of_t<Bs...>;

  template <bool... Bs>
  constexpr bool any_of_v = (Bs || ...);

  template <bool... Bs>
  using any_of_t = bool_t<any_of_v<Bs...>>;

  template <bool... Bs>
  using or_t = any_of_t<Bs...>;

  template <bool... Bs>
  constexpr bool none_of_v = !(Bs || ...);

  template <bool... Bs>
  using none_of_t = bool_t<none_of_v<Bs...>>;


  template <bool... Bs>
  constexpr all_of_t<Bs...> and_ = {};

  template <bool B0, bool B1>
  constexpr bool_t<B0 && B1> operator&&(bool_t<B0>, bool_t<B1>) { return {}; }


  template <bool... Bs>
  constexpr any_of_t<Bs...> or_ = {};

  template <bool B0, bool B1>
  constexpr bool_t<B0 || B1> operator||(bool_t<B0>, bool_t<B1>) { return {}; }


  template <bool B>
  using not_t = bool_t<!B>;

  template <bool B>
  constexpr bool_t<!B> not_ = {};

  template <bool B>
  constexpr bool_t<!B> operator!(bool_t<B>) { return {}; }


  namespace Impl
  {
    template <class T, T... Is>
    struct IsEqualImpl;

    template <class T, T I0, T... Is>
    struct IsEqualImpl<T, I0, Is...>
        : public std::is_same<std::integer_sequence<T,I0,Is...>,
                              std::integer_sequence<T,Is...,I0>> {};

    template <class T>
    struct IsEqualImpl<T> { enum { value = true }; };

  } // end namespace Impl

  template <class T, T... values>
  using IsEqual = Impl::IsEqualImpl<T,values...>;

  template <class T, class... Ts>
  using is_one_of = or_t<std::is_same_v<T, Ts>...>;

} // end namespace AMDiS
