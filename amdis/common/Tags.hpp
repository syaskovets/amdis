#pragma once

namespace AMDiS
{
  namespace tag
  {
    // some tags representing traits classes or categories
    struct none {};
    struct scalar {};
    struct vector {};
    struct matrix {};
    struct unknown {};

    struct defaulted {};

  } // end namespace tag

} // end namespace AMDiS
