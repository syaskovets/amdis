#pragma once

namespace AMDiS
{
  namespace Assigner
  {
    struct assign
    {
      template <class T, class S>
      constexpr void operator()(T const& from, S&& to) const
      {
        to = from;
      }
    };

    struct plus_assign
    {
      template <class T, class S>
      constexpr void operator()(T const& from, S&& to) const
      {
        to += from;
      }
    };

    struct minus_assign
    {
      template <class T, class S>
      constexpr void operator()(T const& from, S&& to) const
      {
        to -= from;
      }
    };

    struct multiplies_assign
    {
      template <class T, class S>
      constexpr void operator()(T const& from, S&& to) const
      {
        to *= from;
      }
    };

    struct divides_assign
    {
      template <class T, class S>
      constexpr void operator()(T const& from, S&& to) const
      {
        to /= from;
      }
    };

  } // end namespcae Assigner
} // end namespace AMDiS
