#pragma once

#include <cmath>

#include <amdis/operations/Basic.hpp>

/// Macro that generates a unary functor with explicitly given function and derivative.
/**
 *  \p NAME    Name of the class.
 *  \p FCT     Name of a unary c++-function that represents the functor.
 *  \p DERIV   Name of a unary c++-function that represents the derivative of this functor.
 */
#ifndef AMDIS_MAKE_UNARY_FUNCTOR
#define AMDIS_MAKE_UNARY_FUNCTOR( NAME, FCT, DERIV )          \
    struct NAME                                               \
    {                                                         \
      template <class T>                                      \
      constexpr auto operator() (T const& x) const            \
      {                                                       \
        return FCT ;                                          \
      }                                                       \
      struct Derivative                                       \
      {                                                       \
        template <class T>                                    \
        constexpr auto operator() (T const& x) const          \
        {                                                     \
          return DERIV ;                                      \
        }                                                     \
      };                                                      \
      friend constexpr auto partial(NAME const&, index_t<0>)  \
      {                                                       \
        return Derivative{} ;                                 \
      }                                                       \
    };
#endif


namespace AMDiS
{
  namespace Operation
  {
    /** \addtogroup operations
     *  @{
     **/

    /// Functor that represents the signum function
    struct Signum
    {
      template <class T>
      constexpr auto operator()(T const& x) const
      {
        return (x > T{0} ? T{1} : T{-1});
      }

      friend constexpr auto partial(Signum const&, index_t<0>)
      {
        return Zero{};
      }
    };

    // generated unary functors using a macro...
    // approximate polynomial order

#ifndef DOXYGEN
    AMDIS_MAKE_UNARY_FUNCTOR( Ceil,  std::ceil(x),  0.0 )
    AMDIS_MAKE_UNARY_FUNCTOR( Floor, std::floor(x), 0.0 )
    AMDIS_MAKE_UNARY_FUNCTOR( Sqrt,  std::sqrt(x),  1.0/(2.0 * std::sqrt(x)) )
    AMDIS_MAKE_UNARY_FUNCTOR( Exp,   std::exp(x),   std::exp(x) )
    AMDIS_MAKE_UNARY_FUNCTOR( Log,   std::log(x),   1.0/x )
    AMDIS_MAKE_UNARY_FUNCTOR( Sin,   std::sin(x),   std::cos(x) )
    AMDIS_MAKE_UNARY_FUNCTOR( Cos,   std::cos(x),   -std::sin(x) )
    AMDIS_MAKE_UNARY_FUNCTOR( Tan,   std::tan(x),   1.0 + Math::sqr(std::tan(x)) )
    AMDIS_MAKE_UNARY_FUNCTOR( Asin,  std::asin(x),  1.0/std::sqrt(1.0 - Math::sqr(x)) )
    AMDIS_MAKE_UNARY_FUNCTOR( Acos,  std::acos(x), -1.0/std::sqrt(1.0 - Math::sqr(x)) )
    AMDIS_MAKE_UNARY_FUNCTOR( Atan,  std::atan(x),  1.0/(1.0 + Math::sqr(x)) )
    AMDIS_MAKE_UNARY_FUNCTOR( Sinh,  std::sinh(x),  std::cosh(x) )
    AMDIS_MAKE_UNARY_FUNCTOR( Cosh,  std::cosh(x),  std::sinh(x) )
    AMDIS_MAKE_UNARY_FUNCTOR( Tanh,  std::tanh(x),  1.0 - Math::sqr(std::tanh(x)) )
    AMDIS_MAKE_UNARY_FUNCTOR( Asinh, std::asinh(x), 1.0/std::sqrt(1.0 + Math::sqr(x)) )
    AMDIS_MAKE_UNARY_FUNCTOR( Acosh, std::acosh(x), 1.0/std::sqrt(-1.0 + Math::sqr(x)) )
    AMDIS_MAKE_UNARY_FUNCTOR( Atanh, std::atanh(x), 1.0/(1.0 - Math::sqr(x)) )
#endif

    /// Binary functor representing the cmath function std::atan2(v)
    struct Atan2
    {
      template <class T1, class T2>
      constexpr auto operator() (T1 const& x, T2 const& y) const
      {
        return std::atan2(x, y);
      }

      // -y/(Math::sqr(x) + Math::sqr(y))
      constexpr friend auto partial(Atan2 const&, index_t<0>)
      {
        return compose(Divides{}, compose(Negate{}, Arg<1>{}),
          compose(Plus{}, compose(Pow<2>{}, Arg<0>{}), compose(Pow<2>{}, Arg<1>{})));
      }

      // x/(Math::sqr(x) + Math::sqr(y));
      constexpr friend auto partial(Atan2 const&, index_t<1>)
      {
        return compose(Divides{}, Arg<0>{},
          compose(Plus{}, compose(Pow<2>{}, Arg<0>{}), compose(Pow<2>{}, Arg<1>{})));
      }
    };


    template <class T>
    struct Clamp
    {
      constexpr Clamp(T const& lo, T const& hi)
        : lo_(lo)
        , hi_(hi)
      {
        // assert(lo < hi);
      }

      constexpr auto operator() (T const& v) const
      {
        return v < lo_ ? lo_ : hi_ < v ? hi_ : v;
      }

      struct DClamp
      {
        constexpr auto operator() (T const& v) const
        {
          return v < lo_ ? T(0) : hi_ < v ? T(0) : T(1);
        }

        T lo_, hi_;
      };

      constexpr friend auto partial(Clamp const& c, index_t<0>)
      {
        return DClamp{c.lo_,c.hi_};
      }

      T lo_, hi_;
    };

    /** @} **/

  } // end namespace Operation
} // end namespace AMDiS
