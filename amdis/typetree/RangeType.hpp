#pragma once

#include <type_traits>
#include <utility>

#include <dune/common/fvector.hh>
#include <dune/common/tuplevector.hh>
#include <dune/common/typetraits.hh>

#include <amdis/common/Index.hpp>
#include <amdis/common/TypeTraits.hpp>

namespace Dune
{
  template <class... T>
  struct FieldTraits<TupleVector<T...>>
  {
    using field_type = std::common_type_t<typename FieldTraits<T>::field_type...>;
    using real_type = std::common_type_t<typename FieldTraits<T>::real_type...>;
  };
}

namespace AMDiS
{
  namespace Impl
  {
    template <class Node, class R, class NodeTag>
    struct RangeTypeImpl
    {
      static_assert( Dune::AlwaysFalse<NodeTag>::value, "Unknown node-type for range definition" );
    };
  }

  /// \brief Range type of a node in the basis tree, composed of the leaf basis
  /// range types.
  /**
   * Generate the range type by recursively combining leaf range types to a
   * hybrid node range type. Range types for PowerNodes are thereby constructed
   * as Dune::FieldVector of child range types. CompositeNodes produce a
   * TupleVector of the difference child range types.
   *
   * \tparam Node  Type of a basis-tree node
   * \tparam R     Coefficient type [double]
   **/
  template <class Node, class R = double>
  using RangeType_t =
    typename Impl::RangeTypeImpl<Node, R, typename Node::NodeTag>::type;


  namespace Impl
  {
    // Leaf node
    template <class Node, class R>
    struct RangeTypeImpl<Node, R, Dune::TypeTree::LeafNodeTag>
    {
      using LocalBasis = typename Node::FiniteElement::Traits::LocalBasisType;
      using T = typename LocalBasis::Traits::RangeType;
      using type = remove_cvref_t<decltype(std::declval<R>() * std::declval<T>())>;
    };

    // Power node
    template <class Node, class R>
    struct RangeTypeImpl<Node, R, Dune::TypeTree::PowerNodeTag>
    {
      using ChildNode = typename Node::template Child<0>::type;

      template <bool childIsLeaf, class ChildRangeType>
      struct FlatType {
        using type = Dune::FieldVector<ChildRangeType, int(Node::degree())>;
      };

      template <class T>
      struct FlatType<true, Dune::FieldVector<T,1>> {
        using type = Dune::FieldVector<T, int(Node::degree())>;
      };

      using type = typename FlatType<ChildNode::isLeaf, RangeType_t<ChildNode,R>>::type;
    };

    // Composite node
    template <class Node, class R>
    struct RangeTypeImpl<Node, R, Dune::TypeTree::CompositeNodeTag>
    {
      template <class Idx> struct RangeTypeGenerator;
      template <std::size_t... I>
      struct RangeTypeGenerator<Indices<I...>>
      {
        template <std::size_t J>
        using ChildNode = typename Node::template Child<J>::type;
        using type = Dune::TupleVector<RangeType_t<ChildNode<I>,R>...>;
      };

      using type = typename RangeTypeGenerator<std::make_index_sequence<std::size_t(Node::degree())>>::type;
    };

  } // end namespace Impl

} // end namespace AMDiS
