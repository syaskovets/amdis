#pragma once

#include <dune/typetree/traversal.hh>

namespace AMDiS {
namespace Traversal {

  using Dune::TypeTree::applyToTree;
  using Dune::TypeTree::forEachNode;
  using Dune::TypeTree::forEachLeafNode;

}} // end namespace AMDiS::Traversal
