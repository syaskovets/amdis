#pragma once

#include <fstream>
#include <set>
#include <string>

#include <dune/common/parametertree.hh>

namespace AMDiS
{
  /// Parser for AMDiS initfile format
  /**
   * An AMDiS initfile is a list of pairs `(paramter, value)`
   * stored in a structured textfile of the format
   * \verbatim
   * parameter: value          % a comment
   *
   * #include <filename>       % include files
   * var_${parameter}: value2  % variable replacement in keys
   * parameter2: ${parameter}  % variable replacement in values
   *
   * parameter3: 1 + sin(4.0)  % expressions in the values
   * parameter4: $(1 + 2)      % expression replacement
   * \endverbatim
   **/
  class InitfileParser
  {
  public:
    /// Read initfile from input stream into parameter-tree
    static void readInitfile(std::istream& in, Dune::ParameterTree& pt, bool overwrite);

    /// Read initfile from input stream into parameter-tree
    static void readInitfile(std::string const& fn, Dune::ParameterTree& pt, bool overwrite = true);

  private:
    /// Provide a list of already read files. This is necessary to not double include the same file.
    static std::set<std::string>& includeList()
    {
      static std::set<std::string> includeFiles;
      return includeFiles;
    }

    /// \brief Replace variables by its value stored in the parameter-tree
    /**
     * Replaces variables of the form ${variablename} or $variablename in the `input`
     * by a corresponding value already stored in the parameter tree `pt`.
     */
    static std::string replaceVariable(Dune::ParameterTree const& pt, std::string const& input);

    /// \brief Evaluate an expression. NOTE: currently not implemented
    /**
     * Evaluates expressions of the form $(expression) in the `input`
     **/
    static std::string replaceExpression(Dune::ParameterTree const& pt, std::string const& input);
  };

} // end namespace AMDiS
