#pragma once

#include <amdis/common/Concepts.hpp>
#include <amdis/gridfunctions/GridFunction.hpp>
#include <amdis/interpolators/AverageInterpolator.hpp>
#include <amdis/interpolators/LocalAverageInterpolator.hpp>
#include <amdis/interpolators/SimpleInterpolator.hpp>

namespace AMDiS
{
  /// \brief Interpolate given function in discrete function space restricted to
  /// DOFs in the bitVector marked with `true`.
  template <class Tag = tag::assign, class B, class C, class F, class BV,
    REQUIRES(Concepts::GlobalBasis<B>),
    REQUIRES(Concepts::AnyGridFunction<F>)>
  void interpolate(const B& basis, C& coeff, F&& f, const BV& bitVector)
  {
    auto gridFunction = makeGridFunction(FWD(f), basis.gridView());
    auto interpolator = InterpolatorFactory<Tag>::create(basis.rootBasis(), basis.prefixPath());
    interpolator(coeff, gridFunction, bitVector);
  }

  /// \brief Interpolate given function in discrete function space
  template <class Tag = tag::assign, class B, class C, class F,
    REQUIRES(Concepts::GlobalBasis<B>),
    REQUIRES(Concepts::AnyGridFunction<F>)>
  void interpolate(const B& basis, C& coeff, F&& f)
  {
    auto gridFunction = makeGridFunction(FWD(f), basis.gridView());
    auto interpolator = InterpolatorFactory<Tag>::create(basis.rootBasis(), basis.prefixPath());
    interpolator(coeff, gridFunction);
  }

} // end namespace AMDiS
