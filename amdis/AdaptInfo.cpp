#include <config.h>

#include "AdaptInfo.hpp"

// std c++ headers
#include <iostream>
#include <string>

#include <amdis/Initfile.hpp>

namespace AMDiS
{

  AdaptInfo::ScalContent::ScalContent(std::string const& prefix)
  {
    Parameters::get(prefix + "->tolerance", spaceTolerance);
    Parameters::get(prefix + "->time tolerance", timeTolerance);
    Parameters::get(prefix + "->time relative tolerance", timeRelativeTolerance);
    Parameters::get(prefix + "->coarsen allowed", coarsenAllowed);
    Parameters::get(prefix + "->refinement allowed", refinementAllowed);
    Parameters::get(prefix + "->sum factor", fac_sum);
    Parameters::get(prefix + "->max factor", fac_max);

    if (timeTolerance == 0.0 && timeRelativeTolerance == 0.0)
      timeTolerance = 1.0;
    timeErrLow = timeTolerance * 0.3;
  }


  AdaptInfo::AdaptInfo(std::string const& name)
    : name_(name)
  {
    // init();
    Parameters::get(name + "->start time", startTime_);
    time_ = startTime_;

    Parameters::get(name + "->timestep", timestep_);
    Parameters::get(name + "->end time", endTime_);
    Parameters::get(name + "->max iteration", maxSpaceIteration_);
    Parameters::get(name + "->max timestep iteration", maxTimestepIteration_);
    Parameters::get(name + "->max time iteration", maxTimeIteration_);
    Parameters::get(name + "->min timestep", minTimestep_);
    Parameters::get(name + "->max timestep", maxTimestep_);
    Parameters::get(name + "->number of timesteps", nTimesteps_);
    Parameters::get(name + "->time tolerance", globalTimeTolerance_);
  }


  void AdaptInfo::printTimeErrorLowInfo() const
  {
    for (auto const& scalContent : scalContents_)
    {
      auto i = scalContent.first;
      std::cout << "    Time error estimate     ["<<i<<"] = "
                << timeEstCombined(i) << "\n"
                << "    Time error estimate sum ["<<i<<"] = "
                << scalContent.second.est_t_sum << "\n"
                << "    Time error estimate max ["<<i<<"] = "
                << scalContent.second.est_t_max << "\n"
                << "    Time error low bound    ["<<i<<"] = "
                << scalContent.second.timeErrLow << "\n"
                << "    Time error high bound   ["<<i<<"] = "
                << scalContent.second.timeTolerance << "\n";
    }
  }

  void AdaptInfo::reset()
  {
    spaceIteration_ = -1;
    timestepIteration_ = 0;
    timeIteration_ = 0;
    time_ = 0.0;
    timestep_ = 0.0;
    timestepNumber_ = 0;
    solverIterations_ = 0;
    solverResidual_ = 0.0;

    Parameters::get(name_ + "->timestep", timestep_);
    lastProcessedTimestep_ = timestep_;
  }

} // end namespace AMDiS
