#include <config.h>

#include "Environment.hpp"

#if AMDIS_HAS_PETSC
#include <petscsys.h>
#endif

// AMDiS includes
#include <amdis/Initfile.hpp>
#include <amdis/Output.hpp>

/// Level bound for output of additional information. See \ref info() and \ref info_()
#ifndef AMDIS_INFO_LEVEL
  #define AMDIS_INFO_LEVEL 1
#endif

/// Print messages on all ranks in parallel
#ifndef AMDIS_MSG_ALL_RANKS
  #define AMDIS_MSG_ALL_RANKS 0
#endif

namespace AMDiS
{
  Environment::Environment(std::string const& initFileName)
  {
    Parameters::clearData();
    if (!initFileName.empty())
      Parameters::init(initFileName);
  }


  Environment::Environment(int& argc, char**& argv, std::string const& initFileName)
    : Environment(initFileName)
  {
#if AMDIS_HAS_PETSC
    PetscInitialize(&argc, &argv, PETSC_NULL, PETSC_NULL);
    petscInitialized_ = true;
#endif

    auto& mpi = Mpi::instance();
    mpi.registerMpiHelper(Dune::MPIHelper::instance(argc, argv));

    Parameters::clearData();
    if (initFileName.empty()) {
      if (argc > 1)
        Parameters::init(argv[1]);
      else
        warning("No initfile specified. Using default values for all parameters.");
    }
  }


  Environment::~Environment()
  {
#if AMDIS_HAS_PETSC
    if (petscInitialized_)
      PetscFinalize();
#endif
  }

  // Return the info level for messages in `info()`
  int Environment::infoLevel()
  {
    static int level = AMDIS_INFO_LEVEL;
    Parameters::get("level of information", level);
    return level;
  }

  // Return whether all ranks print `msg()`
  bool Environment::msgAllRanks()
  {
    static bool msg = AMDIS_MSG_ALL_RANKS;
    Parameters::get("output all ranks", msg);
    return msg;
  }

} // end namespace AMDiS
