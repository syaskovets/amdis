#pragma once

#include <amdis/Output.hpp>
#include <amdis/common/Math.hpp>
#include <amdis/common/Apply.hpp>

namespace AMDiS
{
  /// Polynomial order of local basis-node
  template <class N,
    class = typename N::NodeTag>
  int order(N const& node)
  {
    if constexpr (N::isLeaf)
      return node.finiteElement().localBasis().order();
    else if constexpr (N::isPower)
      return order(node.child(0u));
    else if constexpr (N::isComposite)
      return Ranges::applyIndices<std::size_t(N::degree())>([&](auto... ii) {
        return Math::max(order(node.child(ii))...);
      });
    else {
      warning("Unknown basis-node type. Assuming polynomial degree 1.");
      return 1;
    }
  }

} // end namespace AMDiS
