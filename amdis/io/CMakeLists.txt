target_sources(amdis PRIVATE
    FileWriterBase.cpp
)

install(FILES
    BackupWriter.hpp
    DuneVtkWriter.hpp
    FileWriterBase.hpp
    FileWriterCreator.hpp
    GmshWriter.hpp
    VTKSequenceWriter.hpp
    VTKWriter.hpp
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/amdis/io)
