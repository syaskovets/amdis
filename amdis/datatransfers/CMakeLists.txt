install(FILES
    InterpolationDataTransfer.hpp
    InterpolationDataTransfer.inc.hpp
    NoDataTransfer.hpp
    SimpleDataTransfer.hpp
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/amdis/datatransfers)
