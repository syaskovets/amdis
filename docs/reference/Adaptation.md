# Adaptation {: #group-adaptation }

This doc page is under construction.

See the reference page on initfile parameters for [`Markers`](../Initfile#markers), [`Grids`](../Initfile#grids) and [`Adaptivity`](../Initfile#adaptivity).

## Data Transfer
During grid adaption, the basis attached to the grid and all data attached to that basis
must be transfered to the new grid. This datatransfer happens in essentially two phases: 1.
the data on the old grid is cached in a persistent container, 2. on the new grid either
the data is simply copied from the cache, or an interpolation from the cached data is
constructed to the new elements.

But, there might be other strategies to transfer the data between grid changes. It is up
to the user to choose a datatransfer strategy. This choice can be done in AMDiS, by
implementing the `DataTransfer` interface, and specializing the `DataTransferFactory` with
a `<tag>` associated to the implement transfer routines.

All `DataTransfer` implementations follow the interface

```c++
/// Definition of the interface a DataTransfer class must fulfill
template <class B, class C>
struct DataTransfer
{
  /// \brief Collect data that is needed before grid adaption.
  /**
   * \param basis         The global basis before the grid is updated.
   * \param container     The original data before grid adaption.
   * \param mightCoarsen  Flag to indicate whether there are elements marked for coarsening.
   **/
  void preAdapt(B const& basis, C const& container, bool mightCoarsen);

  /// \brief Interpolate data to new grid after grid adaption.
  /**
   * \param basis      The global basis after the grid is updated.
   * \param container  The original data vector not yet updated.
   **/
  void adapt(B const& basis, C& container);

  /// \brief Perform cleanup after grid adaption
  /**
   * \param container  The data vector after any adaption and data transfer.
   **/
  void postAdapt(C& container);
};
```

with `B` the type of the global basis amd `C` the type of the coefficient vector.
A class that has this interface can be stored in the type-erasure class `DataTransfer`.

Additionally, all datatransfers specialize the class `DataTransferFactory`:

```c++
template <class Tag>
struct DataTransferFactory
{
  template <class B, class C>
  static DataTransfer<B,C> create(B const& basis, C const& container);
};
```

Examples of `<tag>` that are currently implemented:

- `tag::no_adaption`: Implements the most simple datatransfer that does not transfer the
  values, but just resizes the data container to the updated basis size.
- `tag::interpolation_datatransfer`: This is the default datatransfer, implementing a
  proper interpolation of the data from the old grid to the new grid.
- `tag::simple_datatransfer`: A simplified version of the `tag::interpolation_datatransfer`
  that works only for grids with a single `GeometryType`.

### Changing the Data Transfer strategy
The `<tag>` can be used to choose a strategy for a `DOFVector`. By default the strategy
`tag::interpolation_datatransfer` is set for all `DOFVector`s. But, it might be that
some data does not need to be transfered, or you want to implement your own strategy,
then you can change this default:

```c++
DOFVector vec{...};
vec.setDataTransfer(<tag>);
```

#### Example
In a `ProblemInstat` it might be that you don't want to interpolate the old-solution
to a new grid, since it is rewritten by default in the `initTimestep(AdaptInfo)` method:

```c++
ProblemStat prob{"name", grid, lagrange<2>()};
prob.initialize(INIT_ALL);

// choose the simple data transfer for the solution vector
prob.solutionVector()->setDataTransfer(tag::simple_datatransfer{});

ProblemInstat probInstat{"name", prob};
probInstat.initilize(INIT_ALL);

// disable all interpolation for the old-solution vector
probInstat.oldSolutionVector()->setDataTransfer(tag::no_datatransfer{});
```