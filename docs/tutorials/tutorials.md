# Tutorial

This tutorial covers some simple but common examples for discretizing PDEs and
pre-/post processing the discrete data on a grid.

1. [Grids and Discrete Functions](grids-and-discretefunctions.md):
Learn how to define a grid as a discretization of the domain and a discrete function
of that grid. How to tranverse the grid elements and assign data to the discrete
functions.

2. [Working with the Problem class](../reference/Problem.md):
Create a problem class as a container for the grid, basis, solution vector and
linear system and many more. What can a problem class do for your workflow.

3. [Backup and Restore](backup-restore.md)
Write intermediate states of your lengthy conmputations to file and resume the
simulation from a backup file.

4. [Operators](../reference/Operators.md):
Define your PDE in terms of operator terms. Learn how to add operators to a
problem and how to define your own operators.

5. [Block Matrices and Vectors](block-mat-vec.md)
Use block matrices and vectors instead of the default flat versions.

6. [Boundary Conditions](../reference/Constraints.md):
Select parts of the grid boundary and assign boundary conditions or boundary
operators. Learn how to specify the boundary parts and how to set different
types of boundary conditions, like Dirichlet or periodic conditions, or
Neumann/Robin-type boundary integrals.

7. [Linear Solvers](../reference/Solvers.md):
Control the solution of the assembled linear system, which linear solver to use
and which preconditioner. How to control solver parameters and how to add an
own linear solver.

8. [Adaptivity and Grid Refinement](../reference/Adaptation.md):
Grid adaptivity needs control of marking and refinement strategies. Learn how to
define and add a marker to the problem, learn about interpolation during grid
adaptivity and how to load balance your grid.

9. [FileWriters](../reference/Filewriters.md):
Output and backup/restore of data is an important topic for simulations and
for postprocessing. Different Writers are introduced and the configuration and control
of these writers discussed.
