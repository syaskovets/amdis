#include <amdis/AdaptInfo.cpp>
#include <amdis/AMDiS.hpp>

#include <amdis/common/Literals.hpp>

#include <amdis/typetree/TreePath.hpp>

#include "Tests.hpp"

using namespace AMDiS;

int main()
{
  AdaptInfo adaptInfo("adapt");

  auto root_tp = makeTreePath();
  auto tp = makeTreePath(0_c);
  std::string str = "0";

  adaptInfo.setEstSum(0.1, tp);
  AMDIS_TEST_EQ(adaptInfo.estSum(tp), 0.1);

  adaptInfo.setEstSum(0.2, root_tp);
  AMDIS_TEST_EQ(adaptInfo.estSum(root_tp), 0.2);

  AMDIS_TEST_EQ(adaptInfo.size(), 2);

  adaptInfo.setEstSum(0.3, "0");
  AMDIS_TEST_EQ(adaptInfo.estSum(tp), 0.3);

  adaptInfo.setEstSum(0.4, 0_c);
  AMDIS_TEST_EQ(adaptInfo.estSum(tp), 0.4);

  adaptInfo.setEstSum(0.5, str);
  AMDIS_TEST_EQ(adaptInfo.estSum(tp), 0.5);

  return report_errors();
}