#include <amdis/AMDiS.hpp>
#include <amdis/common/FakeContainer.hpp>

#include "Tests.hpp"

using namespace AMDiS;

void test1()
{
  FakeContainer<int,1> vec1;
  FakeContainer<int,1> vec2(vec1);
  FakeContainer<int,1> vec3(std::move(vec2));

  FakeContainer<int,1> vec4 = vec1;
  [[maybe_unused]] FakeContainer<int,1> vec5 = std::move(vec3);

  vec1.reserve(7);
  vec1.resize(1);
  vec4.resize(1);

  vec1[0] = 0.0;
  vec4[1] = vec1[0];

  vec1.push_back(42);
  vec1.emplace_back(42);

  AMDIS_TEST(!vec1.empty());

  vec1.front() = 1;
}


void test2()
{
  constexpr auto bitSet = FakeContainer<bool,true>{};

  // The bitset is not empty
  AMDIS_TEST(!bitSet.empty());

  // All entries are true
  std::size_t i = 0;
  for (auto it = bitSet.begin(); i < 10; ++it, ++i)
    AMDIS_TEST(*it);

  for (i = 0; i < 10; ++i) {
    AMDIS_TEST(bitSet[i]);
    AMDIS_TEST(bitSet.at(i));
  }

  // The front entry is true
  AMDIS_TEST(bitSet.front());

  // resize has not effect
  AMDIS_TEST(!bitSet.empty());

  // use bitset element as template parameter
  [[maybe_unused]] auto iconst = std::integral_constant<bool, bitSet[0]>{};
}


int main(int argc, char** argv)
{
  Environment env(argc, argv);

  test1();
  test2();

  return report_errors();
}
