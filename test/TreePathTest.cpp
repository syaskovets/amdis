#include <config.h>

#include <dune/common/indices.hh>
#include <amdis/Environment.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/typetree/TreePath.hpp>

#include "Tests.hpp"

using namespace AMDiS;
using namespace Dune::Indices;

template <class... I, class... J,
  std::enable_if_t<(sizeof...(I) == sizeof...(J)), int> =0>
bool compare(Dune::TypeTree::HybridTreePath<I...> const& tp1, Dune::TypeTree::HybridTreePath<J...> const& tp2)
{
  return Ranges::applyIndices<sizeof...(I)>([&](auto... i) {
    return ((Dune::TypeTree::treePathEntry(tp1,i) == Dune::TypeTree::treePathEntry(tp2,i)) &&...);
  });
}


int main (int argc, char** argv)
{
  Environment env(argc, argv);

  static_assert( std::is_same_v<
    Dune::TypeTree::HybridTreePath<>,
    TYPEOF(makeTreePath())> );

  static_assert( std::is_same_v<
    Dune::TypeTree::HybridTreePath<>,
    TYPEOF(makeTreePath(std::declval<RootTreePath>()))> );

  static_assert( std::is_same_v<
    Dune::TypeTree::HybridTreePath<std::size_t>,
    TYPEOF(makeTreePath(0))> );

  static_assert( std::is_same_v<
    Dune::TypeTree::HybridTreePath<std::size_t, std::integral_constant<std::size_t,1>>,
    TYPEOF(makeTreePath(0,_1))> );

  static_assert( std::is_same_v<
    Dune::TypeTree::HybridTreePath<std::size_t, std::integral_constant<std::size_t,1>>,
    TYPEOF(makeTreePath(std::tuple{0,_1}))> );

  auto tp = Dune::TypeTree::hybridTreePath(1u, _2);
  static_assert( std::is_same_v<TYPEOF(tp), TYPEOF(makeTreePath(tp))> );

  AMDIS_TEST(compare(tp, 12_tp));
  AMDIS_TEST(compare(makeTreePath(1,2), 12_tp));
  AMDIS_TEST(compare(makeTreePath(0,1,2), 012_tp));
  AMDIS_TEST(compare(makeTreePath(0,0,1,2), 0012_tp));

  return report_errors();
}
