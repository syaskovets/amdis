#include <amdis/AMDiS.hpp>
#include <amdis/Integrate.hpp>
#include <amdis/ProblemStat.hpp>

#include <dune/grid/onedgrid.hh>
#include <dune/grid/yaspgrid.hh>

#if HAVE_DUNE_SPGRID
#include <dune/grid/spgrid.hh>
#endif
#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif
#if HAVE_DUNE_FOAMGRID
#include <dune/foamgrid/foamgrid.hh>
#endif
#if HAVE_DUNE_UGGRID
#include <dune/grid/uggrid.hh>
#endif

#include "Tests.hpp"

using namespace AMDiS;

template <class Grid, class Factory>
void test(Factory factory)
{
  using namespace Dune::Indices;
  using Param  = TaylorHoodBasis<Grid>;
  using Problem = ProblemStat<Param>;

  std::size_t num_elements = 0;
  std::size_t num_vertices = 0;

  // backup
  {
    std::unique_ptr<Grid> grid(factory());
    grid->loadBalance();

    Problem prob("test", *grid);
    prob.initialize(INIT_ALL);
    prob.globalRefine(2);

    prob.solution(_0) << [](auto const& x) { return x; };
    prob.solution(_1) << [](auto const& x) { return two_norm(x); };

    num_elements = prob.grid()->size(0);
    num_vertices = prob.grid()->size(Grid::dimension);

    AdaptInfo adaptInfo("adapt");
    prob.writeFiles(adaptInfo, true);
  }

  // restore
  {
    Problem prob("test");
    prob.restore(INIT_ALL);

    AMDIS_TEST_EQ(num_elements, prob.grid()->size(0));
    AMDIS_TEST_EQ(num_vertices, prob.grid()->size(Grid::dimension));

    DOFVector<typename Param::GlobalBasis> vec(prob.globalBasis());
    valueOf(vec,_0) << [](auto const& x) { return x; };
    valueOf(vec,_1) << [](auto const& x) { return two_norm(x); };

    double error0 = std::sqrt(integrate(
      unary_dot(prob.solution(_0) - valueOf(vec,_0)), prob.gridView()));
    AMDIS_TEST(error0 < 1.e-10);

    double error1 = std::sqrt(integrate(
      pow<2>(prob.solution(_1) - valueOf(vec,_1)), prob.gridView()));
    AMDIS_TEST(error1 < 1.e-10);
  }
}

template <class Grid>
void test_cube()
{
  using Factory = Dune::StructuredGridFactory<Grid>;
  Dune::FieldVector<double,Grid::dimensionworld> left(0.0);
  Dune::FieldVector<double,Grid::dimensionworld> right(1.0);
  auto sizes = Dune::filledArray<Grid::dimension, unsigned int>(2);
  test<Grid>([&]() { return Factory::createCubeGrid(left, right, sizes); });
}

template <class Grid>
void test_simplex()
{
  using Factory = Dune::StructuredGridFactory<Grid>;
  Dune::FieldVector<double,Grid::dimensionworld> left(0.0);
  Dune::FieldVector<double,Grid::dimensionworld> right(1.0);
  auto sizes = Dune::filledArray<Grid::dimension, unsigned int>(2);
  test<Grid>([&]() { return Factory::createSimplexGrid(left, right, sizes); });
}

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  std::string filename = "test";
  Parameters::set("test->restore->grid", filename + "_" + std::to_string(GRID_ID) + ".grid");
  Parameters::set("test->restore->solution", filename + "_" + std::to_string(GRID_ID) + ".solution");

  Parameters::set("test->output->format", "backup");
  Parameters::set("test->output->filename", filename + "_" + std::to_string(GRID_ID));

#if GRID_ID == 0
  test_cube<Dune::YaspGrid<2>>();
  test_cube<Dune::YaspGrid<3>>();
#elif GRID_ID == 1 && HAVE_DUNE_UGGRID
  test_cube<Dune::UGGrid<2>>();
  test_cube<Dune::UGGrid<3>>();
#elif GRID_ID == 2 && HAVE_DUNE_ALUGRID
  test_cube<Dune::ALUGrid<2,2,Dune::cube,Dune::nonconforming>>();
  test_cube<Dune::ALUGrid<3,3,Dune::cube,Dune::nonconforming>>();
  //test_cube<Dune::ALUGrid<2,3,Dune::cube,Dune::nonconforming>>();
#elif GRID_ID == 3 && HAVE_DUNE_SPGRID
  test<Dune::SPGrid<double,2>>([]() {
    return std::unique_ptr<Dune::SPGrid<double,2>>(
      new Dune::SPGrid<double,2>(Dune::SPDomain<double, 2>({0.0,0.0}, {1.0,1.0}), Dune::SPMultiIndex<2>({2,2}))); });
  test<Dune::SPGrid<double,3>>([]() {
    return std::unique_ptr<Dune::SPGrid<double,3>>(
      new Dune::SPGrid<double,3>(Dune::SPDomain<double, 3>({0.0,0.0,0.0}, {1.0,1.0,1.0}), Dune::SPMultiIndex<3>({2,2,2}))); });
#elif GRID_ID == 4 && HAVE_DUNE_UGGRID
  test_simplex<Dune::UGGrid<2>>();
  test_simplex<Dune::UGGrid<3>>();
#elif GRID_ID == 5 && HAVE_DUNE_ALUGRID
  test_simplex<Dune::ALUGrid<2,2,Dune::simplex,Dune::nonconforming>>();
  test_simplex<Dune::ALUGrid<3,3,Dune::simplex,Dune::nonconforming>>();
  //test_simplex<Dune::ALUGrid<2,3,Dune::simplex,Dune::nonconforming>>();
#elif GRID_ID == 6 && HAVE_DUNE_ALUGRID
  test_simplex<Dune::ALUGrid<2,2,Dune::simplex,Dune::conforming>>();
  test_simplex<Dune::ALUGrid<3,3,Dune::simplex,Dune::conforming>>();
  //test_simplex<Dune::ALUGrid<2,3,Dune::simplex,Dune::conforming>>();
#elif GRID_ID == 7
  test_cube<Dune::OneDGrid>();
#endif
  // test_simplex<Dune::AlbertaGrid<2,2>>(); // Segmentation fault
  // test_simplex<Dune::FoamGrid<2,2>>(); // Segmentation fault

  return report_errors();
}
