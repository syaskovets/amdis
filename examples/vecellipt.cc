#include <config.h>

#include <iostream>

#include <amdis/AMDiS.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/ProblemStat.hpp>

using namespace AMDiS;
using namespace Dune::Functions::BasisFactory;

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  using Grid = Dune::YaspGrid<GRIDDIM>;
  auto grid = MeshCreator<Grid>{"elliptMesh"}.create();
  ProblemStat prob("ellipt", *grid, power<2>(lagrange<2>()));
  prob.initialize(INIT_ALL);

  AdaptInfo adaptInfo("adapt");

  auto opL = makeOperator(tag::gradtest_gradtrial{}, 1.0);
  prob.addMatrixOperator(opL, 1, 1);

  auto opM = makeOperator(tag::test_trial{}, 1.0);
  prob.addMatrixOperator(opM, 0, 0);
  prob.addMatrixOperator(opM, 1, 0);

  auto opForce = makeOperator(tag::test{}, [](auto const& x) { return -1.0; }, 0);
  prob.addVectorOperator(opForce, 0);


  // set boundary condition
  auto predicate = [](auto const& x){ return x[0] < 1.e-8 || x[1] < 1.e-8; }; // define boundary
  auto dbcValues = [](auto const& x){ return 0.0; }; // set value
  prob.addDirichletBC(predicate, 1, 1, dbcValues);

  prob.buildAfterAdapt(adaptInfo, Flag(0));

#ifdef DEBUG_MTL
  // write matrix to file
  if (Parameters::get<int>("elliptMesh->global refinements").value_or(0) < 4) {
    mtl::io::matrix_market_ostream out("matrix.mtx");
    out << prob.systemMatrix().matrix();

    std::cout << prob.systemMatrix().matrix() << '\n';
  }
#endif

  prob.solve(adaptInfo);
  prob.writeFiles(adaptInfo);

  return 0;
}
