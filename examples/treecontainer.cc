#include <amdis/AMDiS.hpp>
#include <amdis/typetree/TreeContainer.hpp>

#include <dune/grid/yaspgrid.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

using namespace AMDiS;

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  // create grid
  Dune::YaspGrid<2> grid({1.0,1.0}, {1,1});
  auto gridView = grid.leafGridView();

  // create basis
  using namespace Dune::Functions::BasisFactory;
  auto basis1 = makeBasis(gridView,
    composite(power<2>(lagrange<2>(), flatInterleaved()), lagrange<1>(), flatLexicographic()));
  auto basis2 = makeBasis(gridView,
    power<2>(power<2>(lagrange<2>(), flatInterleaved()), flatLexicographic()));

  auto localView = basis1.localView();

  auto container = TypeTree::treeContainer<double>(localView.tree());
  auto container2 = TypeTree::treeContainer(localView.tree(),
    [&](auto const& node) { return TypeTree::treeContainer<double>(localView.tree()); });
}
